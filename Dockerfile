# Base image selection
FROM debian:9

# List of ports that will be exposed to the outside world
#EXPOSE 3000

# Installing the services and dependencies
ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8
RUN [ "apt-get", "-q", "update" ]
RUN [ "apt-get", "-qy", "--force-yes", "upgrade" ]
RUN [ "apt-get", "install", "--no-install-recommends", "-qy", "--force-yes", \
      "ca-certificates", \
      "git", \
      "cron", \
      "msmtp" ]
RUN [ "apt-get", "clean" ]
RUN [ "rm", "-rf", "/var/lib/apt/lists/*", "/tmp/*", "/var/tmp/*" ]

RUN adduser --quiet --disabled-password --shell /bin/bash --home /home/reminder --gecos "User" reminder

USER reminder

WORKDIR /home/reminder

# Add latest master branch version to skip loading from cache if newer commit available
ADD https://git.fsfe.org/api/v1/repos/max.mehl/reminder-mails/branches/master version.json

RUN git clone https://git.fsfe.org/max.mehl/reminder-mails.git

WORKDIR /home/reminder/reminder-mails

# Add crontab file in the cron directory
ADD cron.txt /etc/cron.d/reminder-mails

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/reminder-mails

# CMD to run the service on container start
CMD cron && tail -f /home/reminder/reminder-mails/app.log
