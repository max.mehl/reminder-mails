#!/bin/bash

basedir="${0%/*}"
LOG="${basedir}/app.log"
touch "${LOG}"
touch "${basedir}/msmtp.log"

exec > >(tee -ia "${LOG}")
exec 2>&1

function decho {
  DATE=$(date +%y-%m-%d_%H:%M:%S)
  echo "[$DATE] $*"
}

#exec >> "${LOG}" 2>&1

# If argument is given, check for mail template and send it out
if [[ ! -z "$1" ]]; then
  if [[ -e "${basedir}/mails/$1" ]]; then
    cat "${basedir}/mails/$1" | sed "1s|^|Date: $(date -R)\n|"  | sed "1s|^|Message-ID: <ReMiNdEr.$(date +%s)@fsfe.org>\n|" | msmtp -C "${basedir}"/.msmtprc -t --read-envelope-from -a fsfe
    if [[ "$?" == 0 ]]; then
      decho "[INFO] Mail \"$1\" has been sent successfully"
    else
      decho "[ERROR] There has been an error with sending \"$1\"".
    fi
  else
    decho "[ERROR] Mail template \"$1\" does not exist."
    exit 1
  fi
else
  decho "[WARNING] No argument given. Will not send a mail."
  exit 0
fi
